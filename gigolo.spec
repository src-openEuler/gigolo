%global minorversion 0.5

Name:           gigolo
Version:        0.5.3
Release:        1
Summary:        frontend to manage connections to remote filesystems using GIO/GVfs
License:        GPLv2
URL:            http://goodies.xfce.org/projects/applications/gigolo/
Source0:        http://archive.xfce.org/src/apps/%{name}/%{minorversion}/%{name}-%{version}.tar.bz2

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  gtk3-devel
BuildRequires:  desktop-file-utils
BuildRequires:  gcc

Requires:       gvfs-fuse3

%description
Gigolo is a frontend to easily manage connections to remote filesystems
using GIO/GVfs. It allows you to quickly connect/mount a remote filesystem
and manage bookmarks of such.

%prep
%setup -q

%build
#rm -f waf
%configure
%make_build

%install
%make_install
%find_lang %{name}

# remove duplicate docs
rm -rf %{buildroot}/%{_datadir}/doc/gigolo

desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop

%find_lang %{name}

%files -f %{name}.lang
%license COPYING
%doc AUTHORS ChangeLog NEWS TODO THANKS
%{_bindir}/gigolo
%{_datadir}/icons/hicolor/*/apps/org.xfce.gigolo.*
%{_datadir}/applications/gigolo.desktop
%{_mandir}/man1/gigolo.1.gz

%changelog
* Mon Jan 15 2024 zhangxingrong <zhangxingrong@uniontech.com> - 0.5.3-1
- upgrade to 0.5.3

* Fri Feb 18 2022 zhanglin <lin.zhang@turbolinux.com.cn> - 0.5.2-2
- fix Require: gvfs-fuse 

* Fri Jun 18 2021 zhanglin <lin.zhang@turbolinux.com.cn> - 0.5.2-1
- Update to 0.5.2

* Tue Jul 28 2020 Dillon Chen <dillon.chen@turbolinux.com.cn> - 0.5.1-1
- Init package
